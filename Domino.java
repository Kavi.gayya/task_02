package base;

public class Domino implements Comparable<Domino> {

    private int high;
    private int low;
    private int hx;
    private int hy;
    private int lx;
    private int ly;
    private boolean placed = false;

    public Domino(int high, int low) {
        this.high = high;
        this.low = low;
    }

    public void place(int hx, int hy, int lx, int ly) {
        this.hx = hx;
        this.hy = hy;
        this.lx = lx;
        this.ly = ly;
        placed = true;
    }

    public boolean isPlaced() {
        return placed;
    }

    public String toString() {
        StringBuilder result = new StringBuilder("[")
                .append(high)
                .append(low)
                .append("]");

        if (!placed) {
            result.append("unplaced");
        } else {
            result.append("(")
                    .append(hx + 1)
                    .append(",")
                    .append(hy + 1)
                    .append(")")
                    .append("(")
                    .append(lx + 1)
                    .append(",")
                    .append(ly + 1)
                    .append(")");
        }

        return result.toString();
    }

    public void invert() {
        int tx = hx;
        hx = lx;
        lx = tx;

        int ty = hy;
        hy = ly;
        ly = ty;
    }

    public boolean isHorizontal() {
        return hy == ly;
    }

    @Override
    public int compareTo(Domino other) {
        if (this.high != other.high) {
            return Integer.compare(other.high, this.high);
        }
        return Integer.compare(other.low, this.low);
    }
}
