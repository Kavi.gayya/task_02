package base;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Scanner;

public final class IOLibrary {

    private static final Scanner scanner = new Scanner(System.in);

    private IOLibrary() {
        // Private constructor to prevent instantiation
    }

    public static String readString() {
        return readInput("Enter a string: ");
    }

    public static InetAddress readIPAddress() {
        return readIPInput("Enter an IP address (format: xxx.xxx.xxx.xxx): ");
    }

    private static String readInput(String prompt) {
        System.out.print(prompt);
        return scanner.nextLine();
    }

    private static InetAddress readIPInput(String prompt) {
        do {
            try {
                String[] chunks = readInput(prompt).split("\\.");
                byte[] data = {
                        Byte.parseByte(chunks[0]),
                        Byte.parseByte(chunks[1]),
                        Byte.parseByte(chunks[2]),
                        Byte.parseByte(chunks[3])
                };
                return Inet4Address.getByAddress(data);
            } catch (Exception e) {
                System.out.println("Invalid input. Please enter a valid IP address.");
            }
        } while (true);
    }

    public static void closeScanner() {
        scanner.close();
    }

	public static String getString() {
		// TODO Auto-generated method stub
		return null;
	}
}
