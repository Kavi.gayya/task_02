package base;

import java.net.InetAddress;

/**
 * @author Kevan Buckley, maintained by __student
 * @version 2.0, 2014
 */
public class ConnectionGenius {

    private InetAddress hostAddress;

    public ConnectionGenius(InetAddress hostAddress) {
        this.hostAddress = hostAddress;
    }

    public void startGame() {
        downloadWebVersion();
        connectToWebService();
        readyToPlay();
    }

    private void downloadWebVersion() {
        System.out.println("Getting specialized web version.");
        System.out.println("Please wait a moment.");
    }

    private void connectToWebService() {
        System.out.println("Connecting to web service.");
    }

    private void readyToPlay() {
        System.out.println("Ready to play!");
    }
}
