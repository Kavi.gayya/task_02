package base;

public class _Q {
    static class Author {
        String name;
        String quote;

        public Author(String name, String quote) {
            this.name = name;
            this.quote = quote;
        }
    }

    static final Author[] quotes = {
        new Author("Elbert Hubbard", "Progress comes from the intelligent use of experience."),
        new Author("Albert Einstein", "No amount of experimentation can ever prove me right; a single experiment can prove me wrong."),
        new Author("George MacDonald", "To be trusted is a greater complement than to be loved."),
        new Author("Confucius", "Everything has beauty, but not everyone sees it."),
        new Author("Oliver Wendell Holmes", "Pretty much all the honest truth-telling there is in the world is done by children."),
        new Author("Martin Luther King Jr.", "A lie cannot live."),
        new Author("Winston Churchill", "There are a lot of lies going around...and half of them are true."),
        new Author("Mark Twain", "Everyone is a moon and has a dark side which he never shows to anybody."),
        new Author("Nachman of Bratslav", "Lies are usually caused by an undue fear of men."),
        new Author("Albert Einstein", "If we knew what it was we were doing, it would not be called research, would it?")
    };
}
